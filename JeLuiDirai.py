import socket, ssl, time, os.path, json
class Bot():
    ENCODING = 'utf-8'
    DATA_FILE = 'JeLuiDirai.json'
    def __init__(self, server, port, useSSL, nick, chans, init_cmds=[]):
        if not os.path.isfile(self.DATA_FILE):
            with open(self.DATA_FILE, 'w+') as f:
                f.write('{}')
        self.socket = socket.create_connection((server, port))
        if useSSL:
            self.socket = ssl.wrap_socket(self.socket)
        self.recv()
        self.send("NICK " + nick)
        self.send("USER " + nick + " 0 * :Bot JeLuiDirai de Gnomino")
        time.sleep(0.5)
        self.recv()
        for cmd in init_cmds:
            self.send(cmd)
        for c in chans:
            self.send("JOIN " + c)
        while True:
            data = self.recv()
            for line in data.split('\r\n'):
                self.parse(line)
    def log(self, data):
        print(data)
    def send(self, msg):
        self.socket.send(msg.encode(self.ENCODING) + "\r\n".encode(self.ENCODING))
        self.log("> " + msg)
    def sendMsg(self, msg, chan):
        self.send('PRIVMSG ' + chan + ' :' + msg)
    def recv(self):
        data = self.socket.recv(4096).decode(self.ENCODING, 'ignore')
        self.log(data)
        return data
    def getDB(self):
        with open(self.DATA_FILE, 'r') as f:
            return json.loads(f.read())
    def updateDB(self, data):
        with open(self.DATA_FILE, 'w+') as f:
            f.write(json.dumps(data, indent=2))
    def parse(self, data):
        splitted = data.split(' ', 3)
        if len(splitted) == 4:
            if splitted[1] == 'PRIVMSG':
                sender = splitted[0].split('!')[0][1:]
                channel = splitted[2]
                msg = splitted[3][1:]
                if msg == '!enableJLD':
                    self.send("WHOIS " + sender)
                    data = self.recv()
                    if data.find('is a registered nick') == -1:
                        self.sendMsg("Vous n'êtes pas enregistré(e)", sender)
                        return False
                    else:
                        db = self.getDB()
                        if sender.lower() not in db:
                            db[sender.lower()] = []
                            self.updateDB(db)
                            self.sendMsg("C'est tout bon, je prendrai les messages !", sender)
                        else:
                            self.sendMsg("Je prends déjà vos messages !", sender)
                elif msg == '!disableJLD':
                    self.send("WHOIS " + sender)
                    data = self.recv()
                    if data.find('is a registered nick') == -1:
                        self.sendMsg("Vous n'êtes pas enregistré(e)", sender)
                        return False
                    else:
                        db = self.getDB()
                        if sender.lower() in db:
                            del(db[sender.lower()])
                            self.updateDB(db)
                            self.sendMsg("Bien, bien, si c'est ce que vous voulez ...", sender)
                        else:
                            self.sendMsg("On se connaît ?", sender)
                elif msg == '!help' or msg == '!helpJLD':
                    messages = ['Vous devez être enregistré(e) pour utiliser JeLuiDirai.', '!help : Affiche ceci', '!enableJLD : Active JeLuiDirai', '!send <destinataire> <message> : Enregistre un message', '!disableJLD : Désactive JeLuiDirai']
                    for m in messages:
                        self.sendMsg(m, sender)
                else:
                    msg_splitted = msg.split(' ', 2)
                    if len(msg_splitted) == 3:
                        if msg_splitted[0] == '!send':
                            target = msg_splitted[1].lower()
                            message = msg_splitted[2]
                            self.send("WHOIS " + sender)
                            data = self.recv()
                            if data.find('is a registered nick') == -1:
                                self.sendMsg("Vous n'êtes pas enregistré(e)", sender)
                                return False
                            db = self.getDB()
                            if target in db:
                                db[target].append("De " + sender + " : " + message)
                                self.updateDB(db)
                                self.sendMsg("Je lui dirai à son prochain JOIN !", sender)
                            else:
                                self.sendMsg("C'est qui ?", sender)
        if len(splitted) == 3:
            if splitted[1] == 'JOIN':
                joiner = splitted[0].split('!')[0][1:].lower()
                db = self.getDB()
                if joiner in db:
                    for msg in db[joiner]:
                        self.sendMsg(msg, joiner)
                    db[joiner] = []
                    self.updateDB(db)
        if len(splitted) >= 1:
            if splitted[0] == 'PING':
                self.send('PONG ' + " ".join(splitted[1:]))
Bot('irc.smoothirc.net', 6669, True, 'JeLuiDirai', ['#pantoufle', '#pantoufle-test'], ['PRIVMSG NickServ :IDENTIFY [password]'])
